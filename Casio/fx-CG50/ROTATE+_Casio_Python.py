from math import pi,sin,cos,ceil
from random import random,randint
from casioplot import set_pixel,show_screen

A=0
B=0
SCX=0
SCY=0
STH=0
RN=0
PH=0
PH1=0
SPH2=0
SI=0
CIRC=0
SINPH=0
COSPH=0
SINPH1=0
COSPH1=0
SINPH3=0
COSPH3=0
oldXcoord=0
oldYcoord=0
xcoord=0
ycoord=0
centreX=200
centreY=100
exitCd=0

# Thanks to John Craig and his book Python For Casio for this line function
def line(x1,y1,x2,y2):
#  print(x1,y1)
#  print(x2,y2)
  x=x1
  y=y1
  dx=abs(x2-x1)
  dy=abs(y2-y1)
  sx=1 if x1<x2 else -1 if x1>x2 else 0
  sy=1 if y1<y2 else -1 if y1>y2 else 0
#  print("sxy=",sx,sy)
  ix=dy/2
  iy=dx/2
  pixels=dx+1 if dx>dy else dy+1
  while int(pixels):
#    print(x,int(x),y,int(y))
    set_pixel(int(x),int(y))
    ix+=dx
    if ix>=dy:
      ix-=dy
      x+=sx
    iy+=dy
    if iy>=dx:
      iy-=dx
      y+=sy
    pixels-=1

def drawOneRot():
  global A, B, SCX, SCY, STH, RN, PH, PH1, SPH2, SI, CIRC, SINPH, COSPH, SINPH1, COSPH1, SINPH3, COSPH3, oldXcoord, oldYcoord, xcoord, ycoord, centreX, centreY, exitCd
  colVal=1
  A1=SINPH1
  B1=(COSPH3 * COSPH1) - (SINPH3 * SINPH1)
  W=PH1
  while (W <= (PH1 + CIRC)):
    W=(W + SI)
    A2=A1
    A1=(A1 * B) + (A * B1)
    B1=(B1 * B) - (A2 * A)
    xcoord=centreX + (SCX * RN * B1)
    ycoord=centreY + (SCY * RN * A1)
    try:
#      print(oldXcoord,oldYcoord,xcoord,ycoord)
      line(oldXcoord,oldYcoord,xcoord,ycoord)
    except Exception as e:
      print("exc")
      print(e)
      exitCd=1
      break
    oldXcoord=xcoord
    oldYcoord=ycoord
  PH1=PH1 + PH
  SINPH1NEW=(SINPH * COSPH1) + (COSPH * SINPH1)
  COSPH1=(COSPH * COSPH1) - (SINPH * SINPH1)
  SINPH1=SINPH1NEW
  if (STH != 0):
#    print("sth=",STH)
    RN=SPH2 * (RN / STH)
  else:
#    print("rn= ",RN)
    RN=0

def startR():
  global A, B, SCX, SCY, STH, RN, PH, PH1, SPH2, SI, CIRC, SINPH, COSPH, SINPH1, COSPH1, SINPH3, COSPH3, oldXcoord, oldYcoord, xcoord, ycoord, centreX, centreY
  C=0
  SIDES=randint(3,7)
  RADS=180 / pi
#  print("rads=",RADS)
  PH=((random() * 7) + 3) / RADS
  SINPH=sin(PH)
  COSPH=cos(PH)
  PH1=(random() * 360) / RADS
  SINPH1=sin(PH1)
  COSPH1=cos(PH1)
  TH=(90 - (180 / SIDES)) / RADS
#  print("th=",TH)
  STH=sin(TH)
  PH2=pi - PH - TH
  SPH2=sin(PH2)
  RN=2
  CIRC=400 / RADS
  SI=(2 * pi) / SIDES
  B=cos(SI)
  A=sin(SI)
  SCX=(random() * 3) + 1
  SCY=(random() * 3) + 1
  PH3=pi * (randint(0,2) - 1)
  COSPH3=cos(PH3)
  SINPH3=sin(PH3)
  oldXcoord=centreX + (SCX * RN * COSPH1)
  oldYcoord=centreY + (SCY * RN * SINPH1)

startR()
ct=0
while (abs(xcoord) < 300):
  if (exitCd != 0):
    break
#  if (ct>30):
#    break
#  ct=ct+1
  drawOneRot()
#print(abs(xcoord))
show_screen()
