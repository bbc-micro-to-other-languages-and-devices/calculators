' There is no handy way of transferring text files to a CG50 and load in to the program editor. 
' The best that can be done is to upload it to the calculator, find it in the file system from the editor and brace yourself for a load of edits!
' Function names may or may not be correctly recognised and characters like π and → will be removed as the font is not the same as on the calculator
ClrGraph
GridOff
AxesOff
CoordOff
LabelOff
LocusOff
ViewWindow -400,400,1,-300,300,1
Rad
{12,1}→Dim Mat A
Fill(0,Mat A)
RanInt#(0,4)+3→Mat A[1,1]
180÷π→Mat A[2,1]
((Ran# x7)+3)÷Mat A[2,1]→Mat A[3,1]
sin(Mat A[3,1])→J
cos(Mat A[3,1])→K
(Ran# x360)÷Mat A[2,1]→G
sin(G)→H
cos(G)→I
(90-(180÷Mat A[1,1]))÷Mat A[2,1]→mat A[9,1]
sin (Mat A[9,1])→Mat A[4,1]
π-Mat A[3,1]-Mat A[9,1]→Mat A[5,1]
sin(Mat A[5,1])→Mat A[10,1]
2→P
400÷Mat A[2,1]→Mat A[8,1]
(2xπ)÷Mat A[1,1]→L
cos (L)→M
sin (L)→N
(Ran# x3)+1→O
(Ran# x3)+1→Mat A[6,1]
πx(RanInt(0,2)-1)→Mat A[7,1]
cos (Mat A[7,1])→U
sin (Mat A[7,1])→V
0→Q
0→R
Q+(OxPxI)→S
R+(Mat A[6,1]xPxH)→T
PlotOn S,T
Lbl 1
H→A
(UxI)-(VxH)→B
G→W
G+Mat A[8,1]→Z
While W≤Z
W+L→W
A→C
(AxM)+(NxB)→A
(BxM)-(CxN)→B
Q+(OxPxB)→X
R+(Mat A[6,1]xPxA)→Y
F-Line S,T,X,Y
X→S
Y→T
WhileEnd
G+Mat A[3,1]→G
(JxI)+(KxH)→D
(KxI)-(JxH)→I
D→H
If (Mat A[4,1]≠0)
Then
Mat A[10,1]x(P÷Mat A[4,1])→P
Else
0→P
IfEnd
If (Abs(X)<555)
Then
Goto 1
IfEnd
