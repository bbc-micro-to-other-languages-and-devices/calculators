from math import pi,trunc,sin,cos
from random import random
from matplotlib.pyplot import plot,show,axis

A = 0
B = 0
SCX = 0
SCY = 0
STH = 0
RN = 0
PH = 0
PH1 = 0
SPH2 = 0
SI = 0
CIRC = 0
SINPH = 0
COSPH = 0
SINPH1 = 0
COSPH1 = 0
SINPH3 = 0
COSPH3 = 0
oldXcoord = 0
oldYcoord = 0
xcoord = 0
ycoord = 0
centreX = 0
centreY = 0
exitCd = 0

def drawOneRot():
  global A, B, SCX, SCY, STH, RN, PH, PH1, SPH2, SI, CIRC, SINPH, COSPH, SINPH1, COSPH1, SINPH3, COSPH3, oldXcoord, oldYcoord, xcoord, ycoord, centreX, centreY, exitCd
  colVal = 1
  A1 = SINPH1
  B1 = (COSPH3 * COSPH1) - (SINPH3 * SINPH1)
  W = PH1
  while (W <= (PH1 + CIRC)):
    W = (W + SI)
    A2 = A1
    A1 = (A1 * B) + (A * B1)
    B1 = (B1 * B) - (A2 * A)
    xcoord = centreX + (SCX * RN * B1)
    ycoord = centreY + (SCY * RN * A1)
    try:
      plot((oldXcoord,xcoord),(oldYcoord,ycoord))
    except Exception as e:
      print(e)
      exitCd = 1
      break
    oldXcoord = xcoord
    oldYcoord = ycoord
  PH1 = PH1 + PH
  SINPH1NEW = (SINPH * COSPH1) + (COSPH * SINPH1)
  COSPH1 = (COSPH * COSPH1) - (SINPH * SINPH1)
  SINPH1 = SINPH1NEW
  if (STH != 0):
    RN = SPH2 * (RN / STH)
  else:
    RN = 0

def startR():
  global A, B, SCX, SCY, STH, RN, PH, PH1, SPH2, SI, CIRC, SINPH, COSPH, SINPH1, COSPH1, SINPH3, COSPH3, oldXcoord, oldYcoord, xcoord, ycoord, centreX, centreY
  C = 0
  SIDES = trunc((random() * 7) + 3)
  RADS = 180 / pi
  PH = ((random() * 7) + 3) / RADS
  SINPH = sin(PH)
  COSPH = cos(PH)
  PH1 = (random() * 360) / RADS
  SINPH1 = sin(PH1)
  COSPH1 = cos(PH1)
  TH = (90 - (180 / SIDES)) / RADS
  STH = sin(TH)
  PH2 = pi - PH - TH
  SPH2 = sin(PH2)
  RN = 2
  CIRC = 400 / RADS
  SI = (2 * pi) / SIDES
  B = cos(SI)
  A = sin(SI)
  SCX = (random() * 3) + 1
  SCY = (random() * 3) + 1
  PH3 = pi * trunc((random() * 2) - 1)
  COSPH3 = cos(PH3)
  SINPH3 = sin(PH3)
  oldXcoord = centreX + (SCX * RN * COSPH1)
  oldYcoord = centreY + (SCY * RN * SINPH1)
  if (SIDES < 5):
    axis((-100,100,-100,100))
  else:
    axis((-10,10,-10,10))
startR()
while (abs(xcoord) < 700):
  if (exitCd != 0):
    break
  drawOneRot()
print(abs(xcoord))
axis(False)
show()
